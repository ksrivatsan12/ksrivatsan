package com.atlassian.commons;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class Page extends GlobalVars {

	// private WebDriver driver = super.driver;
	public void setTextWithClear(WebElement element, String value) {
		element.click();
		element.sendKeys(Keys.chord(Keys.CONTROL, "a", Keys.DELETE));
		Assert.assertEquals(element.getText(), "");
		element.sendKeys(value);
	}

	public void setText(WebElement element, String value) {
		//element.click();

		element.sendKeys(value);
		
	}

	public void Click(WebElement element) {

		element.click();
	
	}

	public void jsClick(WebElement element) {

		((JavascriptExecutor) driver).executeScript("arguments[0].click();", element);
	}

	public void btnClick(WebElement element) {

		try {

			Click(element);
		}

		catch (StaleElementReferenceException e) {
			Click(element);
		}
	}

	public void waitForElement(WebElement element) {

		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}

	public void pressEnter(WebElement element) {

		element.sendKeys(Keys.ENTER);
	}

	public void pressTab(WebElement element) {

		element.sendKeys(Keys.TAB);
	}

	public void selectByValue(WebElement element, String value) {

		Select select = new Select(element);
		select.selectByVisibleText(value);
	}

}
