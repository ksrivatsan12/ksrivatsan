package com.atlassian.commons;

import org.openqa.selenium.WebDriver;

public class GlobalVars {

	public  WebDriver driver;
	public static String projectDir = System.getProperty("user.dir");
	public static String URL = "https://cesinterview.atlassian.net/secure/RapidBoard.jspa?projectKey=SEL&rapidView=1";

	
	public WebDriver getDriver() {
		
		return driver;
	}
}
