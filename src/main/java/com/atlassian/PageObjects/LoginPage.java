package com.atlassian.PageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.atlassian.commons.Page;

public class LoginPage extends Page {

	private static WebDriver driver;
	
	public LoginPage(WebDriver driver) {
		
		this.driver = driver;
		PageFactory.initElements(driver, this);
		
	}
	
	@FindBy(id = "username")
	WebElement txt_username; 
	

	@FindBy(id = "password")
	WebElement txt_password; 
	

	@FindBy(xpath = "//span[contains(text(),'Continue')]")
	WebElement btn_Continue; 
	

	@FindBy(xpath = "//span[contains(text(),'Log in')]")
	WebElement btn_Login; 
	
	public void Login(String username, String password) {
		
		setTextWithClear(txt_username, username);
		Click(btn_Continue);
		setTextWithClear(txt_password, password);
		pressEnter(txt_password);
		//Click(btn_Login);

	}
	
}
