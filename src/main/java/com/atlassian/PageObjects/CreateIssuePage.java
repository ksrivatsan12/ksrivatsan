package com.atlassian.PageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.atlassian.commons.Page;

public class CreateIssuePage extends Page {

	private static WebDriver driver;

	public CreateIssuePage(WebDriver driver) {

		this.driver = driver;
		PageFactory.initElements(driver, this);

	}

	@FindBy(id = "issuetype-field")
	WebElement slct_IssueType;


	@FindBy(id = "summary")
	WebElement txt_Summary;


	@FindBy(id = "create-issue-submit")
	WebElement btn_Create;

	@FindBy(className = "js-issue-link")
	WebElement issueNumber;

	public void selectIssueType(String value) {
		selectByValue(slct_IssueType, value);
	}
	
	public void waitForElement(WebElement element) {

		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}

	
	public void createIssue(String issueType, String summary) {
		//selectIssueType(issueType);
		setTextWithClear(slct_IssueType, issueType);
		waitForElement(txt_Summary);

		pressTab(slct_IssueType);
		waitForElement(txt_Summary);
		Click(txt_Summary);
		Click(txt_Summary);
	
		setTextWithClear(txt_Summary, summary);
		Click(btn_Create);
	}
	public String getIssueNumber() {
		waitForElement(issueNumber);
		return issueNumber.getText();
	}

}
