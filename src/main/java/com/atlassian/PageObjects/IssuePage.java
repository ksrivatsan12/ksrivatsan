package com.atlassian.PageObjects;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.atlassian.commons.Page;

public class IssuePage extends Page {

	private static WebDriver driver;

	public IssuePage(WebDriver driver) {

		this.driver = driver;
		PageFactory.initElements(driver, this);

	}

	@FindBy(xpath = "//span[contains(@aria-label,'Link issue')]")
	WebElement lnk_LinkIssue;

	/*
	 * @FindBy(className = "css-17qmjyt-singleValue") WebElement txt_LinkedIssues;
	 * 
	 * @FindBy(xpath = "//div[contains(text(),'Search for issues')]") WebElement
	 * txt_SearchIssues;
	 */

	@FindBy(className = "css-17qmjyt-singleValue")
	WebElement emt_LinkedIssues;

	@FindBy(xpath = "//input[contains(@id,'react-select')]")
	WebElement txt_LinkedIssues;

	@FindBy(id = "react-select-3-input")
	WebElement txt_SearchIssues;

	@FindBy(xpath = "//span[(text() ='Link')]")
	WebElement btn_Link;

	@FindBy(css = "h3.sc-kBMPsl.dujzUD")
	WebElement emt_LinkedHeader;

	@FindBy(css = "a.sc-eXsVQl.jYWCLS")
	WebElement lnk_IssueId;

	public void waitForElement(WebElement element) {

		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}

	public void waitForElementInvisible(WebElement element) {

		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.invisibilityOf(element));
	}

	public void waitForElementVisibility(WebElement element) {

		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.visibilityOf(element));
	}

	public void linkIssue_Blocks(String issueId) {

		Click(lnk_LinkIssue);
		waitForElement(emt_LinkedIssues);
		// ((JavascriptExecutor)
		// driver).executeScript("arguments[0].innerHTML='blocks';", emt_LinkedIssues);
		setText(txt_LinkedIssues, "blocks");
		pressEnter(txt_LinkedIssues);
		waitForElement(txt_SearchIssues);
		setText(txt_SearchIssues, issueId);
		pressEnter(txt_SearchIssues);
		waitForElement(btn_Link);

		/*
		 * driver.findElement(By.xpath("//span[contains(text(),'" + issueId +
		 * "')]")).click(); ((JavascriptExecutor)
		 * driver).executeScript("arguments[0].innerHTML='" + issueId + "';",
		 * txt_SearchIssues);
		 */
		// ((JavascriptExecutor)driver).executeScript("arguments[0].click();",driver.findElement(By.xpath("//div[contains(text(),'"+issueId+"')]")));

		// pressEnter(driver.findElement(By.xpath("//div[contains(text(),'"+issueId+"')]")));
		/*
		 * Robot robot = null; try { robot = new Robot(); } catch (AWTException e) { //
		 * TODO Auto-generated catch block e.printStackTrace(); }
		 * 
		 * robot.keyPress(KeyEvent.VK_ENTER); robot.keyRelease(KeyEvent.VK_ENTER);
		 */

		Click(btn_Link);
		try {
			waitForElement(btn_Link);
		} catch (TimeoutException e) {

		}

	}

	public void verifyLinkedIssue(String blockType, String issueId) {

		//waitForElementVisibility(emt_LinkedHeader);
		Assert.assertEquals(emt_LinkedHeader.getText(), blockType);
		Assert.assertEquals(lnk_IssueId.getText(), issueId);

	}

}
