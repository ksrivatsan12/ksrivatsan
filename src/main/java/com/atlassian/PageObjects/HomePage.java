package com.atlassian.PageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.atlassian.commons.Page;

public class HomePage extends Page {

	private static WebDriver driver;
	
	public HomePage(WebDriver driver) {
		
		this.driver = driver;
		PageFactory.initElements(driver, this);
		
	}
	
	@FindBy(id = "createGlobalItem")
	WebElement btn_CreateIssue; 
		
	
	@FindBy(id = "quickSearchGlobalItem")
	WebElement btn_SearchIssue; 
		
	@FindBy(xpath = "//input[@placeholder = 'Search Jira']")
	WebElement txt_IssueSearch; 
	
	public void createIssue() {
		
/*		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Click(btn_CreateIssue);*/
		btnClick(btn_CreateIssue);
		
	}
	
	public void searchIssue(String issueNmbr) {
		
		Click(btn_SearchIssue);
		setTextWithClear(txt_IssueSearch, issueNmbr);
		pressEnter(txt_IssueSearch);
	}

	
	
}
