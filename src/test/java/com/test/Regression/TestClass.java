package com.test.Regression;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.atlassian.PageObjects.CreateIssuePage;
import com.atlassian.PageObjects.HomePage;
import com.atlassian.PageObjects.IssuePage;
import com.atlassian.PageObjects.LoginPage;

public class TestClass extends BaseTest {

	private static String bugIssue;
	private static String storyIssue;
	
	@BeforeClass
	void beforeClass() {
		loginPage = new LoginPage(driver);
		homePage = new HomePage(driver);
		createIssuePage = new CreateIssuePage(driver);
		issuePage = new IssuePage(driver);
		
		driver.get(URL);
		loginPage.Login("karthiks2085@gmail.com", "Atlass12!");

	}

	@BeforeMethod
	void beforeMethod() {


	}

	@AfterMethod
	void afterMethod() {

		//driver.close();
	}

	@Test(priority = 0)
	public void createStoryIssue() {

		homePage.createIssue();
		createIssuePage.createIssue("Story", "This is an User Story");
		System.out.println("Story Issue numner is "+createIssuePage.getIssueNumber());
		storyIssue = createIssuePage.getIssueNumber();

	}

	@Test(priority = 5)
	public void createBugIssue() {

		driver.navigate().refresh();
		homePage.createIssue();
		createIssuePage.createIssue("Bug", "This is a Bug");
		System.out.println("Bug Issue numner is "+createIssuePage.getIssueNumber());
		bugIssue = createIssuePage.getIssueNumber();
	}
	
	@Test(priority = 10)
	public void linkBugIssue() {
		
		homePage.searchIssue(bugIssue);
		issuePage.linkIssue_Blocks(storyIssue);
		
	}
	
	
	@Test(priority = 15)
	public void verifyStoryIssue() {
		
		homePage.searchIssue(storyIssue);
		issuePage.verifyLinkedIssue("is blocked by", bugIssue);
		
	}


}
