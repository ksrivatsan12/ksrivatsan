package com.test.Regression;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeSuite;

import com.atlassian.PageObjects.CreateIssuePage;
import com.atlassian.PageObjects.HomePage;
import com.atlassian.PageObjects.IssuePage;
import com.atlassian.PageObjects.LoginPage;
import com.atlassian.commons.GlobalVars;

public class BaseTest extends GlobalVars {

	public LoginPage loginPage;
	public HomePage homePage;
	public IssuePage issuePage;
	public CreateIssuePage createIssuePage;

	@BeforeSuite(alwaysRun = true)
	public void beforeSuite() {

		System.setProperty("webdriver.chrome.driver", projectDir + "//src//main//resources//chromedriver.exe");

		driver = new ChromeDriver();

		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.manage().window().maximize();

	}

	public void waitForPageLoad(WebElement element) {

		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.elementToBeClickable(element));
		
	}

}
